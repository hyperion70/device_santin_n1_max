Device Tree for Santin N1 MAX
===========================================

Discussion https://forum.smartsworld.ru/view/proshivki-santin-n1-max.1364/

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Octa-core 2.392 GHz 8xCortex-A53
Chipset | MediaTek MT6757 (helio p25)
GPU     | Mali-T880 MP2
Memory  | 6GB RAM
Shipped Android Version | 7.1.1
Storage | 128GB
MicroSD | Up to 128GB
Battery | Li-Pol 3.0 Ah battery
Display | 1080 x 2160 pixels 6.0 inches
Camera  | Main 13MP / Front 13MP, autofocus, LED flash

Copyright 2018 - The Lineage-OS Project.

Thanks to xen0n, Zormax, darklord4822, lalitjoshi06, Adryy, olegsvs, Decker, danielhk, mdeejay, MAD, SRT.


Working:
1. Touch
2. Backlight
3. Fingerprint
4. Audio
5. Camera (photo / video)
6. WiFi
7. Proximity & light sensors
8. Accelerometer

Not working:
1. AudioFX
2. Video playback 
3. RIL
